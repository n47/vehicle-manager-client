package com.n47.vehiclemanagerclient.service;

import com.n47.vehiclemanagerclient.domain.external.model.Vehicle;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Handles communication with Vehiclemanager microservice
 */
@Service
@RequiredArgsConstructor
public class VehicleService {

    @Value("${microservice.vehicle.baseurl}")
    private String vehicleMicroserviceBaseUrl;

    private final RestTemplate restTemplate;


    public Vehicle getVehicleFromMicroservice(String registrationPlate) {
        ResponseEntity<Vehicle> response = restTemplate.exchange(
                getVehicleEndpointUrl(),
                HttpMethod.GET,
                new HttpEntity<>(registrationPlate),
                typeRefVehicleResponse,
                registrationPlate);

        return response.getBody();
    }

    public Vehicle saveVehicleFromMicroservice(Vehicle vehicle) {
        ResponseEntity<Vehicle> response = restTemplate.exchange(
                saveVehicleEndpointUrl(),
                HttpMethod.POST,
                new HttpEntity<>(vehicle),
                typeRefVehicleResponse,
                vehicle);

        return response.getBody();
    }

    private final ParameterizedTypeReference<Vehicle> typeRefVehicleResponse = new ParameterizedTypeReference<>() {
    };

    private String getVehicleEndpointUrl() {
        return vehicleMicroserviceBaseUrl + "/vehicle/get?registrationPlate={registrationPlate}";
    }

    private String saveVehicleEndpointUrl() {
        return vehicleMicroserviceBaseUrl + "/vehicle/add";
    }

}

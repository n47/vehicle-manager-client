package com.n47.vehiclemanagerclient.domain.external.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.springframework.validation.annotation.Validated;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateVehicleRequest {

    private String vehicleType;
    private String registrationPlate;
    private int seatsCount;
    private String category;
    private double price;
    private String currency;
    private boolean available;
}

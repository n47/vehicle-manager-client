package com.n47.vehiclemanagerclient.ctrl;

import com.n47.vehiclemanagerclient.domain.external.model.CreateVehicleRequest;
import com.n47.vehiclemanagerclient.domain.external.model.Vehicle;
import com.n47.vehiclemanagerclient.service.VehicleService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Tag(name = "demo", description = "Demo controller for calling Vehiclemanager microservice's endpoints")
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/demo")
public class DemoCtrl {

    private final VehicleService vehicleService;

    @GetMapping(path = "/vehicle")
    public Vehicle getVehicle(@RequestParam String registrationPlate) {
        return vehicleService.getVehicleFromMicroservice(registrationPlate);
    }

    @PostMapping(path = "/vehicle/save")
    public void saveVehicle(@RequestBody CreateVehicleRequest request) {
        vehicleService.saveVehicleFromMicroservice(new Vehicle()
                .vehicleType(Vehicle.VehicleTypeEnum.fromValue(request.getVehicleType()))
                .registrationPlate(request.getRegistrationPlate())
                .seatsCount(request.getSeatsCount())
                .category(Vehicle.CategoryEnum.fromValue(request.getCategory()))
                .price(request.getPrice())
                .currency(Vehicle.CurrencyEnum.fromValue(request.getCurrency()))
                .available(request.isAvailable())
        );
    }

}
